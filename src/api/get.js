import axios from 'axios';
import apiKey from './config';

export default function (section) {
  const baseUrl = 'https://api.nytimes.com/svc/topstories/v2/';
  const getSectionUrl = `${baseUrl}${section}.json`;
  const getSectionUrlWithApiKey = `${getSectionUrl}?${apiKey}`;
  return axios.get(getSectionUrlWithApiKey);
}
