import React, { Component } from 'react';
import {
  BrowserRouter as Router, Route, Link, Redirect,
} from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import getNews from '../api/get';
import './App.css';
import NYTimesLogo from '../components/NYTimesLogo';
import NewsGroup from '../components/NewsGroup';

library.add(faAngleDown, faAngleUp);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scienceAndTechNews: [],
      healthNews: [],
      politicsNews: [],
      worldNews: [],
    };
  }

  componentDidMount() {
    this.fetchNews();
  }

  getScienceNews() {
    return getNews('science');
  }

  getTechnologyNews() {
    return getNews('technology');
  }

  getHealthNews() {
    return getNews('health');
  }

  getPoliticsNews() {
    return getNews('politics');
  }

  getWorldNews() {
    return getNews('world');
  }

  getNewsPromises() {
    return [
      this.getScienceNews(),
      this.getTechnologyNews(),
      this.getHealthNews(),
      this.getPoliticsNews(),
      this.getWorldNews(),
    ];
  }

  getGroupOfNews(oneNewsType, anotherNewsType) {
    return [...oneNewsType, ...anotherNewsType];
  }

  getResults(responses) {
    const [
      { data: { results: scienceNews } },
      { data: { results: technologyNews } },
      { data: { results: healthNews } },
      { data: { results: politicsNews } },
      { data: { results: worldNews } },
    ] = responses;
    const scienceAndTechNews = this.getGroupOfNews(scienceNews, technologyNews);
    this.setState({
      scienceAndTechNews, healthNews, politicsNews, worldNews,
    });
  }

  fetchNews() {
    Promise
      .all(this.getNewsPromises())
      .then(responses => this.getResults(responses));
  }

  render() {
    const {
      scienceAndTechNews,
      healthNews,
      politicsNews,
      worldNews,
    } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          New York Times - Top News
        </header>
        <Router>
          <div className="App-content">
            <nav className="AppNavigation">
              <ol className="AppNavigationList">
                <li className="AppNavigationListItem">
                  <Link to="/nyttop">Home</Link>
                </li>
                <li className="AppNavigationListItem">
                  <Link to="/science&tech">Science & Tecnology</Link>
                </li>
                <li className="AppNavigationListItem">
                  <Link to="/health">Health</Link>
                </li>
                <li className="AppNavigationListItem">
                  <Link to="/politics">Politics</Link>
                </li>
                <li className="AppNavigationListItem">
                  <Link to="/world">World</Link>
                </li>
              </ol>
            </nav>
            <Route
              path="/nyttop"
              component={() => <NewsGroup />}
            />
            <Route
              path="/science&tech"
              component={() => <NewsGroup newsGroup={scienceAndTechNews} title="Science & Tecnology" />}
            />
            <Route
              path="/health"
              component={() => <NewsGroup newsGroup={healthNews} title="Health" />}
            />
            <Route
              path="/politics"
              component={() => <NewsGroup newsGroup={politicsNews} title="Politics" />}
            />
            <Route
              path="/world"
              component={() => <NewsGroup newsGroup={worldNews} title="World" />}
            />
            <Route
              path="*"
              component={() => <Redirect to="/nyttop" />}
            />
          </div>
        </Router>
        <footer className="App-footer"><NYTimesLogo /></footer>
      </div>
    );
  }
}

export default App;
