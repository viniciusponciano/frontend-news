import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './NewsItem.css';

class NewsItem extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  render() {
    const { news, index } = this.props;
    const {
      title, section, short_url: id, byline, published_date: publishedDate, abstract,
    } = news;
    const { open } = this.state;
    return (
      <div
        className="NewsItemContent"
        {...id}
        key={section + id + index}
        typeof="button"
        onClick={() => this.setState({ open: !open })}
      >
        <h2 className="NewsTitle">
          {title}
        </h2>
        <FontAwesomeIcon icon={open ? 'angle-up' : 'angle-down'} />
        {open && (
        <p className="NewsAbstract">
          {abstract}
          {' '}
          <a href={id}>Read more.</a>
        </p>
        )}
        <p className="NewsByline">
          {byline}
          {publishedDate && ` - ${new Date(publishedDate).toLocaleDateString()}`}
        </p>
      </div>
    );
  }
}


NewsItem.propTypes = {
  news: PropTypes.shape({
    abstract: PropTypes.string,
    section: PropTypes.string,
    short_url: PropTypes.string,
    title: PropTypes.string,
    byline: PropTypes.string,
    published_date: PropTypes.string,
  }),
  index: PropTypes.number,
};

NewsItem.defaultProps = {
  news: {
    abstract: '',
    section: '',
    short_url: '',
    title: '',
    byline: '',
    published_date: '',
  },
  index: Number.NaN,
};

export default NewsItem;
