import React from 'react';

function NYTimesLogo() {
  return (
    <img
      src="https://developer.nytimes.com/files/poweredby_nytimes_200c.png?v=1539041473000"
      alt="NY Times logo"
    />
  );
}

export default NYTimesLogo;
