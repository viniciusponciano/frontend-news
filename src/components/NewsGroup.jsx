import React from 'react';
import PropTypes from 'prop-types';
import NewsItem from './NewsItem';
import './NewsGroup.css';

function renderNewsItem(news, index) {
  return (
    <NewsItem
      news={news}
      index={index}
    />
  );
}

function NewsGroup({ newsGroup, title }) {
  if (!newsGroup || !title) {
    return (
      <h1 className="App-content-title-sector">
          Select in navigation the news section of your interest.
      </h1>
    );
  }
  return (
    <div key={`${title}Title`}>
      <h1 key={`${title}Title`} className="App-content-title-sector">{title}</h1>
      <div key={`${title}News`} className="App-content-sector">
        {newsGroup.map(renderNewsItem)}
      </div>
    </div>
  );
}

NewsGroup.propTypes = {
  newsGroup: PropTypes.array,
  title: PropTypes.string,

};

NewsGroup.defaultProps = {
  newsGroup: [],
  title: '',

};

export default NewsGroup;
