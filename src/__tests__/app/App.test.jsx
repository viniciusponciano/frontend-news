import React from 'react';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import App from '../../app/App';

describe('app component', () => {
  it('should renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render correctly in "debug" mode', () => {
    const component = shallow(<App debug />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with no props', () => {
    const component = shallow(<App />);

    expect(component).toMatchSnapshot();
  });

  it('should render banner text correctly with given strings', () => {
    const strings = ['one', 'two'];
    const component = shallow(<App list={strings} />);
    expect(component).toMatchSnapshot();
  });

  it('should not have props', () => {
    const component = mount(<App />);
    expect(component.props()).toMatchObject({});
    component.unmount();
  });

  it('should have state news array empty', () => {
    const component = mount(<App />);
    const expectedInitialState = {
      healthNews: [], politicsNews: [], scienceAndTechNews: [], worldNews: [],
    };
    expect(component.state()).toMatchObject(expectedInitialState);
    component.unmount();
  });
});
