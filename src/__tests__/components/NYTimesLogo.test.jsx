import React from 'react';
import ReactDOM from 'react-dom';
import NYTimesLogo from '../../components/NYTimesLogo';

describe('NY Times logo', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NYTimesLogo />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});
