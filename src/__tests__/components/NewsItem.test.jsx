import React from 'react';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import NewsItem from '../../components/NewsItem';

describe('news item', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NewsItem />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render correctly in "debug" mode', () => {
    const component = shallow(<NewsItem debug />);
    expect(component).toMatchSnapshot();
  });

  it('should not render correctly with no props', () => {
    const component = shallow(<NewsItem />);
    expect(component).toThrowErrorMatchingSnapshot();
  });

  it('should not render correctly with wrong props', () => {
    const strings = ['one', 'two'];
    const component = shallow(<NewsItem list={strings} />);
    expect(component).toThrowErrorMatchingSnapshot();
  });

  it('should not have props', () => {
    const component = mount(<NewsItem />);
    expect(component.props()).toMatchObject({});
    component.unmount();
  });

  it('should have props', () => {
    const expectedProps = {
      news: {
        title: 'Title',
        section: 'Section',
        id: 'http://www.section.com',
        byline: 'BY TEST',
        published_date: new Date().toISOString(),
        abstract: 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum'
        + ' lorem ipsum lorem ipsum lorem ipsum lorem ipsum',
      },
      index: 0,
    };
    const component = mount(
      <NewsItem {...expectedProps} />,
    );
    expect(component.props()).toMatchObject(expectedProps);
    component.unmount();
  });

  it('should be possible to click any news link', () => {
    const props = {
      news: {
        title: 'Title',
        section: 'Section',
        id: 'http://www.section.com',
        byline: 'BY TEST',
        published_date: new Date().toISOString(),
        abstract: 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum'
        + ' lorem ipsum lorem ipsum lorem ipsum lorem ipsum',
      },
      index: 0,
    };
    const component = mount(
      <NewsItem {...props} />,
    );
    component
      .find('a')
      .simulate('click');
    expect(component).toMatchSnapshot();
    component.unmount();
  });

  it('should have state open inittialy false', () => {
    const component = mount(
      <NewsItem />,
    );
    expect(component.state('open')).toBe(false);
    component.unmount();
  });

  it('should have state open after news click to be true', () => {
    const component = mount(
      <NewsItem />,
    );
    component
      .find('div.NewsItemContent')
      .simulate('click');
    expect(component).toMatchSnapshot();
    expect(component.state('open')).toBe(true);
    component.unmount();
  });
});
