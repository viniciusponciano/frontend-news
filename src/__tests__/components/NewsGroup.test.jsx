import React from 'react';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import NewsGroup from '../../components/NewsGroup';

describe('news group component', () => {
  it('should renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NewsGroup />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render correctly in "debug" mode', () => {
    const component = shallow(<NewsGroup debug />);
    expect(component).toMatchSnapshot();
  });

  it('should render correctly with no props', () => {
    const component = shallow(<NewsGroup />);

    expect(component).toMatchSnapshot();
  });

  it('should render banner text correctly with given strings', () => {
    const strings = ['one', 'two'];
    const component = shallow(<NewsGroup list={strings} />);
    expect(component).toMatchSnapshot();
  });

  it('should have props', () => {
    const expectedProps = { newsGroup: [{ a: '', b: '' }], title: '' };
    const component = mount(<NewsGroup {...expectedProps} />);
    expect(component.props()).toMatchObject(expectedProps);
    component.unmount();
  });

  it('should not have state', () => {
    const component = mount(<NewsGroup />);
    expect(() => component.state()).toThrow();
    component.unmount();
  });

  it('should render title correctly', () => {
    const props = { newsGroup: [{ a: '', b: '' }], title: 'Title' };
    const component = mount(<NewsGroup {...props} />);
    const title = component.find('h1').text();
    expect(title).toEqual(props.title);
    component.unmount();
  });

  it('should render newsItem correctly', () => {
    const props = { newsGroup: [{ a: '', b: '' }], title: 'Title' };
    const component = mount(<NewsGroup {...props} />);
    const newsItem = component.find('div.App-content-sector');
    console.log('chegou', newsItem)
    expect(newsItem).toBeDefined();
    component.unmount();
  });
});
