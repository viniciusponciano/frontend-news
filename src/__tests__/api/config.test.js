import apiKey from '../../api/config';

it('api key is set up', () => {
  expect.assertions(1);
  expect(apiKey).toEqual(expect.stringMatching(/^api-key=[A-z0-9]{32}$/));
});