import getMethod from '../../api/get';

it('api get methods fail', async () => {
  expect.assertions(0);
  let retorno = false;
  try {
    const response = await getMethod();
    expect(response).toBeUndefined();
  } catch (e) {
    retorno = true;
  }
  return retorno;
}, 10000);

it('api get methods not fail', async () => {
  expect.assertions(1);
  const response = await getMethod('politics');
  expect(response).toBeDefined();
}, 10000);