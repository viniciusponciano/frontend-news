run:
	npm start

run-docker:
	docker ps -aq
	docker build -t frontend-news .
	docker run -p 3001:3001 --name frontend-news frontend-news

test:
	npm test

stop-docker:
	docker stop frontend-news
	docker rm frontend-news

remove-build:
	rm -rf ./build

build: remove-build
	npm run build
	serve -s build

lint:
	npm rum lint-fix