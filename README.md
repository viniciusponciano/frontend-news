# Frontend News

Frontend project to present New York Times news by sections. It's avaiable the Science and Technology, Health, Politics and World news. Navigate on each one to see the section top histories.

None of frontend framework was used except for the React, this choice was made to show the news more like the NY Times website. Althought this was not entirely possible some by the lack of time, some by the resources not free used by NYT.

This project is available on gitlab on a private repository, to access please send-me a email to `viniciusponciano@gmail.com` with your user and the access will be granted to you.

**This project has test coverage**

This project has test configured an it shows the coverage. The covered code lines and files is not 100%, some files was done by Facebook Create React App framework and I understand they was massively tested.
Some code lines was not tested by my personal knowledge limitation, I was not able to understand how to test. I'm sorry for that.
All tests is on **`/src/__test__`** folder. This is organized like the tested components structure.<br>

**Git and Gitflow**

This project was build using git to version control the code and to maintaining all history.
The Gitflow help the feature build and to delivery releases with version tags of the code. All branches was merged to master and it's fully functional.

## MAKE and DOCKER

This project has configured a Dockerfile and a Makefile to assist the developers with some basic scripts and tasks.

In this project directory, you can make:

### `make run`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `make run-docker`

Runs the app in the development mode on docker container.<br>
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

This command is usefull to run the project in the same enviroment that they initially wes developed.

### `make stop-docker`

Eventually you will stop the `make run-docker`, but the container will stil setup your docker machine. To clean up this project container use this recipe.

### `make test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
The coverage report will be on console.
This is provided by Jest and Enzyme packages.

### `make build`

Will remove the previous build and make a new one.<br>
Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `make lint`

Will check and fix the code style erros.<br>
This is provided by ESLint package.

## Boilerplate

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

#
This project test entertained me a lot and give me a oportunity to study more about frondend technologies. :)

Thank's for the oportunity and compreension.
#