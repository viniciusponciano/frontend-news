FROM node:10

WORKDIR /usr/src/frontend-news

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3001

CMD ["npm", "run", "start-docker"]